package com.ar.factory;

import org.springframework.stereotype.Service;

import com.ar.exceptions.TipoFiguraException;
import com.ar.model.Circulo;
import com.ar.model.Cuadrado;
import com.ar.model.FiguraTipo;
import com.ar.model.IFigura;
import com.ar.model.Triangulo;

@Service
public class FiguraFactory {

	public IFigura crearFigura (FiguraTipo tipo, Double base, Double altura, Double diametro) {
		IFigura figura = null;

		if(tipo == null) {
			throw new TipoFiguraException("Campo tipo figura es obligatorio");
		} else 
			if (tipo.getNombre().equals("")) {
				throw new TipoFiguraException("Tipo de figura no valida");
		}
			

		switch (tipo) {
		case CIRCULO:
			figura = new Circulo(diametro);
			break;
		case CUADRADO:
			figura = new Cuadrado(base);
			break;
		case TRIANGULO:
			figura = new Triangulo(base, altura);
			break;
		default:
			throw new TipoFiguraException("La Figura no es valida");
		}
		return figura;
	}
}
