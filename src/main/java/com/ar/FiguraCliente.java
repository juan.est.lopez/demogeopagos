package com.ar;

import com.ar.factory.FiguraFactory;
import com.ar.model.FiguraTipo;
import com.ar.model.IFigura;

public class FiguraCliente {

	public static void main(String[] args) {
		FiguraFactory factory = new FiguraFactory();
		IFigura circle = factory.crearFigura(FiguraTipo.CIRCULO, null, null, 20d);
		IFigura square = factory.crearFigura(FiguraTipo.CUADRADO, 20d, 30d, null);
		IFigura triangle = factory.crearFigura(FiguraTipo.TRIANGULO, 50d, 60d, 50d);
		System.out.println(circle.toString());
		System.out.println(square.toString());
		System.out.println(triangle.toString());

	}

}
