package com.ar.exceptions;

public class TipoFiguraException extends RuntimeException{

	private static final long serialVersionUID = 1846450846225106732L;

	public TipoFiguraException(String message) {
		super(message);
	}

}
