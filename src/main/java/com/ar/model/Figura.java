package com.ar.model;

public abstract class Figura {

	private FiguraTipo tipo;
	private Double base;
	private Double altura;
	private Double diametro;

	public Figura(FiguraTipo tipo, Double base, Double altura, Double diametro) {
		super();
		this.tipo = tipo;
		this.base = base;
		this.altura = altura;
		this.diametro = diametro;
	}

	public FiguraTipo getTipo() {
		return tipo;
	}
	public void setTipo(FiguraTipo tipo) {
		this.tipo = tipo;
	}

	public Double getBase() {
		return base;
	}

	public void setBase(Double base) {
		this.base = base;
	}

	public Double getAltura() {
		return altura;
	}

	public void setAltura(Double altura) {
		this.altura = altura;
	}

	public Double getDiametro() {
		return diametro;
	}

	public void setDiametro(Double diametro) {
		this.diametro = diametro;
	}
}
