package com.ar.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Triangulo extends Figura implements IFigura{


	public Triangulo(Double base, Double altura) {
		super(FiguraTipo.TRIANGULO, base, altura, null);
	}

	@Override
	public Double getSuperficie() {
		return (this.getBase() * this.getAltura()) / 2;
	}

	@Override
	public FiguraTipo getTipoFigura() {
		return null;
	}

}
