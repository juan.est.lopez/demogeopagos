package com.ar.model;

public interface IFigura {

	public FiguraTipo getTipoFigura();
	public Double getSuperficie();
	public Double getBase();
	public Double getAltura();
	public Double getDiametro();

}