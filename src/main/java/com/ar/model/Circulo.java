package com.ar.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Circulo extends Figura implements IFigura{

	public Circulo(Double diametro) {
		super(FiguraTipo.CIRCULO, null, null, diametro);
	}

	@Override
	public Double getSuperficie() {
		return Math.PI * Math.pow(this.getDiametro()/2, 2);
	}

	@Override
	public FiguraTipo getTipoFigura() {
		return null;
	}

}