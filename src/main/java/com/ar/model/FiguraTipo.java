package com.ar.model;

public enum FiguraTipo {

	CUADRADO("Cuadrado"), 
	TRIANGULO("Triangulo"), 
	CIRCULO("Circulo");
	
	private final String nombre;
	
	FiguraTipo(String s) {
		this.nombre = s;
	}

	public String getNombre() {
		return nombre;
	}
}
