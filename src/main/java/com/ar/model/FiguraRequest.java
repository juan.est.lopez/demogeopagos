package com.ar.model;

public class FiguraRequest {

	private String superficie;
	private String base;
	private String altura;
	private String diametro; 
	private FiguraTipo tipoFigura;
	
	public FiguraTipo getTipoFigura() {
		return tipoFigura;
	}
	public void setTipoFigura(FiguraTipo tipoFigura) {
		this.tipoFigura = tipoFigura;
	}
	public String getSuperficie() {
		return superficie;
	}
	public void setSuperficie(String superficie) {
		this.superficie = superficie;
	}
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public String getAltura() {
		return altura;
	}
	public void setAltura(String altura) {
		this.altura = altura;
	}
	public String getDiametro() {
		return diametro;
	}
	public void setDiametro(String diametro) {
		this.diametro = diametro;
	}
}