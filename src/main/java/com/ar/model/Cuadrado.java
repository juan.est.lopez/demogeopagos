package com.ar.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Cuadrado extends Figura implements IFigura{

	public Cuadrado(Double base) {
		super(FiguraTipo.CUADRADO, base, null, null);
	}

	@Override
	public FiguraTipo getTipoFigura() {
		return null;
	}

	@Override
	public Double getSuperficie() {
		return this.getBase() * this.getBase();
	}

}
