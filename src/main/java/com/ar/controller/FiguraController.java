package com.ar.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ar.factory.FiguraFactory;
import com.ar.model.FiguraRequest;
import com.ar.model.FiguraResponse;
import com.ar.model.IFigura;

@RestController
public class FiguraController {

	@Autowired
	FiguraFactory figura;

	@PostMapping("/figuras")
	FiguraResponse nuevaFigura(@RequestBody FiguraRequest nuevaFigura) {
		String base = null , altura = null, diametro = null;
		FiguraResponse figuraResponse = new FiguraResponse();
		
		base = validarValor(nuevaFigura.getBase());
		altura = validarValor(nuevaFigura.getAltura());
		diametro = validarValor(nuevaFigura.getDiametro());
		
		IFigura figuraResult = figura.crearFigura(
				nuevaFigura.getTipoFigura(), 
				Double.valueOf(base), 
				Double.valueOf(altura), 
				Double.valueOf(diametro));

		System.out.println(figuraResult.getSuperficie());

		figuraResponse.setTipoFigura(nuevaFigura.getTipoFigura().getNombre());
		
		try {
			figuraResponse.setAltura(nuevaFigura.getAltura());	
		}catch (Exception e) {
		}
		
		try {
			figuraResponse.setBase(nuevaFigura.getBase());	
		} catch (Exception e) {
		}

		try {
			figuraResponse.setSuperficie(figuraResult.getSuperficie().toString());			
		} catch (Exception e) {
		}

		try {
			figuraResponse.setDiametro(nuevaFigura.getDiametro());	
		} catch (Exception e) {
		}
		return  figuraResponse;
	}

	private String validarValor(String valor) {
		String nuevoValor;
		if(valor==null){
			nuevoValor = "0.0";
		} else 
			nuevoValor =valor;
		return nuevoValor;
	}
	

}
